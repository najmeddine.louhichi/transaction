package com.stage.transaction;

import static org.junit.jupiter.api.Assertions.assertThrows;

import com.stage.transaction.dto.TransactionDTO;
import com.stage.transaction.model.Transaction;
import com.stage.transaction.service.TransactionService;
import com.stage.transaction.service.impl.TransactionServiceImpl;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.rules.ExpectedException;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestContextManager;

public class TestAddTransaction {

  @Autowired private TransactionService transactionService;
  private TestContextManager testContextManager;

  @TestConfiguration
  static class TransactionServiceConfiguration {
    @Bean
    public TransactionService transactionService() {
      return new TransactionServiceImpl();
    }
  }

  @Rule public ExpectedException exceptionRule = ExpectedException.none();

  @BeforeEach
  public void beforeEachTestDo() throws Exception {
    this.testContextManager = new TestContextManager(getClass());
    this.testContextManager.prepareTestInstance(this);
    MockitoAnnotations.initMocks(this);
  }

  @DisplayName("Testing add transaction")
  @ParameterizedTest
  @CsvSource({
    "145.1,2020-11-09T19:12:00.312Z,0",
    "145.1,3030-11-09T19:12:00.312Z,1",
    "1456,invalid date,2"
  })
  public void testAddTransactionWithException(String amount, String date, int cas)
      throws Exception {
    TransactionDTO transactionDTO = TransactionDTO.builder().amount(amount).timestamp(date).build();
    Exception exception =
        assertThrows(
            Exception.class,
            () -> {
              transactionService.createNewTransaction(transactionDTO);
            });
    switch (cas) {
      case 0:
        Assert.assertEquals(
            exception.getMessage(), "The provided transaction is older than 60 seconds");
        break;
      case 1:
        Assert.assertEquals(exception.getMessage(), "The given date is a future date");
        break;
      case 2:
        Assert.assertEquals(exception.getMessage(), "Unparsable timestamp");
    }
  }

  @DisplayName("Testing add transaction")
  @ParameterizedTest
  @CsvSource({"145.10"})
  public void testAddTransactionWithoutException(String amount) throws Exception {
    TimeZone tz = TimeZone.getTimeZone("UTC");
    DateFormat df =
        new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.sss'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
    df.setTimeZone(tz);
    String nowAsISO = df.format(new Date());
    TransactionDTO transactionDTO =
        TransactionDTO.builder().amount(amount).timestamp(nowAsISO).build();
    Transaction transaction = transactionService.createNewTransaction(transactionDTO);
    Assert.assertEquals(transaction.getAmount(), new BigDecimal(amount));
  }
}
