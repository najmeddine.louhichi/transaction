package com.stage.transaction;

import com.stage.transaction.dto.StatisticsDTO;
import com.stage.transaction.dto.TransactionDTO;
import com.stage.transaction.service.TransactionService;
import com.stage.transaction.service.impl.TransactionServiceImpl;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.rules.ExpectedException;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestContextManager;

public class TestStatistics {
  @Autowired private TransactionService transactionService;
  private TestContextManager testContextManager;

  @TestConfiguration
  static class TransactionServiceConfiguration {
    @Bean
    public TransactionService transactionService() {
      return new TransactionServiceImpl();
    }
  }

  @Rule public ExpectedException exceptionRule = ExpectedException.none();

  @BeforeEach
  public void beforeEachTestDo() throws Exception {
    this.testContextManager = new TestContextManager(getClass());
    this.testContextManager.prepareTestInstance(this);
    MockitoAnnotations.initMocks(this);
  }

  @DisplayName("Testing add transaction")
  @ParameterizedTest
  @CsvSource({"145.1"})
  public void testStatistics(String amount) throws Exception {
    TimeZone tz = TimeZone.getTimeZone("UTC");
    DateFormat df =
        new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.sss'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
    df.setTimeZone(tz);
    String nowAsISO = df.format(new Date());
    TransactionDTO transactionDTO =
        TransactionDTO.builder().amount(amount).timestamp(nowAsISO).build();
    transactionService.createNewTransaction(transactionDTO);
    transactionService.createNewTransaction(transactionDTO);
    StatisticsDTO statisticsDTO = transactionService.calculateStatistics();
    Assert.assertEquals(statisticsDTO.getAvg(), new BigDecimal("145.10"));
    Assert.assertEquals(statisticsDTO.getMax(), new BigDecimal("145.10"));
    Assert.assertEquals(statisticsDTO.getMin(), new BigDecimal("145.10"));
    Assert.assertEquals(statisticsDTO.getSum(), new BigDecimal("290.20"));
    Assert.assertEquals(statisticsDTO.getCount(), 2);
  }
}
