package com.stage.transaction.exception;

public class OutdatedTransactionException extends Exception {
  public OutdatedTransactionException(String message) {
    super(message);
  }
}
