package com.stage.transaction.exception;

public class UnparsableTransactionException extends Exception {
  public UnparsableTransactionException(String message) {
    super(message);
  }
}
