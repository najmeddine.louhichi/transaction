package com.stage.transaction.dto;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StatisticsDTO {
  private BigDecimal sum;
  private BigDecimal avg;
  private BigDecimal max;
  private BigDecimal min;
  private long count;
}
