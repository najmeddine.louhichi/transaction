package com.stage.transaction.service;

import com.stage.transaction.dto.StatisticsDTO;
import com.stage.transaction.dto.TransactionDTO;
import com.stage.transaction.exception.OutdatedTransactionException;
import com.stage.transaction.exception.UnparsableTransactionException;
import com.stage.transaction.model.Transaction;

public interface TransactionService {

  Transaction createNewTransaction(TransactionDTO transactionDTO)
      throws OutdatedTransactionException, UnparsableTransactionException;

  StatisticsDTO calculateStatistics();

  void deleteTransactions();
}
