package com.stage.transaction.service.impl;

import com.stage.transaction.dto.StatisticsDTO;
import com.stage.transaction.dto.TransactionDTO;
import com.stage.transaction.exception.OutdatedTransactionException;
import com.stage.transaction.exception.UnparsableTransactionException;
import com.stage.transaction.model.Transaction;
import com.stage.transaction.service.TransactionService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.xml.bind.DatatypeConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService {
  private List<Transaction> transactions;

  {
    transactions = new ArrayList<>();
  }

  @Override
  public Transaction createNewTransaction(TransactionDTO transactionDTO)
      throws OutdatedTransactionException, UnparsableTransactionException {
    if (transactionDTO == null
        || transactionDTO.getAmount() == null
        || transactionDTO.getTimestamp() == null) {
      throw new UnparsableTransactionException(
          "One or more of the transactions fields is missing.");
    }
    long timestamp = 0;
    try {
      timestamp =
          DatatypeConverter.parseDateTime(transactionDTO.getTimestamp()).getTime().getTime();
    } catch (Exception e) {
      throw new UnparsableTransactionException("Unparsable timestamp");
    }
    long currentTime = System.currentTimeMillis();
    if (timestamp > currentTime) {
      throw new UnparsableTransactionException("The given date is a future date");
    }
    if (timestamp < currentTime - 1000 * 60) {
      throw new OutdatedTransactionException("The provided transaction is older than 60 seconds");
    }
    BigDecimal amount = new BigDecimal(transactionDTO.getAmount());
    Transaction transaction = Transaction.builder().amount(amount).timestamp(timestamp).build();
    transactions.add(transaction);
    return transaction;
  }

  public StatisticsDTO calculateStatistics() {
    long currentTime = System.currentTimeMillis();
    List<Transaction> validTransactions =
        transactions.stream()
            .filter(transaction -> transaction.getTimestamp() >= currentTime - 1000 * 60)
            .collect(Collectors.toList());
    long count = validTransactions.size();
    BigDecimal sum =
        validTransactions.stream()
            .map(Transaction::getAmount)
            .map(amount -> amount.setScale(2, BigDecimal.ROUND_HALF_UP))
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    BigDecimal max =
        validTransactions.stream()
            .map(Transaction::getAmount)
            .map(amount -> amount.setScale(2, BigDecimal.ROUND_HALF_UP))
            .max(Comparator.naturalOrder())
            .orElse(null);
    BigDecimal min =
        validTransactions.stream()
            .map(Transaction::getAmount)
            .map(amount -> amount.setScale(2, BigDecimal.ROUND_HALF_UP))
            .min(Comparator.naturalOrder())
            .orElse(null);
    Optional<BigDecimal[]> totalWithCountFetch =
        validTransactions.stream()
            .map(Transaction::getAmount)
            .map(amount -> amount.setScale(2, BigDecimal.ROUND_HALF_UP))
            .map(bd -> new BigDecimal[] {bd, BigDecimal.ONE})
            .reduce((a, b) -> new BigDecimal[] {a[0].add(b[0]), a[1].add(BigDecimal.ONE)});
    final BigDecimal[] avg = {null};
    totalWithCountFetch.ifPresent(
        totalWithCount ->
            avg[0] = totalWithCount[0].divide(totalWithCount[1], BigDecimal.ROUND_HALF_UP));
    return StatisticsDTO.builder().sum(sum).avg(avg[0]).max(max).min(min).count(count).build();
  }

  public void deleteTransactions() {
    transactions = new ArrayList<>();
  }
}
