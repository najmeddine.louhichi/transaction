package com.stage.transaction.controller;

import com.stage.transaction.dto.StatisticsDTO;
import com.stage.transaction.dto.TransactionDTO;
import com.stage.transaction.exception.OutdatedTransactionException;
import com.stage.transaction.exception.UnparsableTransactionException;
import com.stage.transaction.model.Transaction;
import com.stage.transaction.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TransactionController {
  @Autowired private TransactionService transactionService;

  @PostMapping("/transactions")
  public ResponseEntity<Transaction> addNewTransaction(@RequestBody TransactionDTO transactionDTO) {
    try {
      return new ResponseEntity<>(
          transactionService.createNewTransaction(transactionDTO), HttpStatus.OK);
    } catch (OutdatedTransactionException e) {
      log.error(e.getMessage());
      return new ResponseEntity(e.getMessage(), HttpStatus.NO_CONTENT);
    } catch (UnparsableTransactionException e) {
      log.error(e.getMessage());
      return new ResponseEntity(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  @GetMapping("statistics")
  public ResponseEntity<StatisticsDTO> calculateStatistics() {
    return new ResponseEntity<>(transactionService.calculateStatistics(), HttpStatus.OK);
  }

  @DeleteMapping("/transactions")
  public ResponseEntity deleteTransactions() {
    transactionService.deleteTransactions();
    return ResponseEntity.noContent().build();
  }
}
